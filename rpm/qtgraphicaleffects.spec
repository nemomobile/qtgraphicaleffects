Name:       qt5-qtgraphicaleffects
Summary:    Qt Graphical Effect
Version:    5.12.7
Release:    1%{?dist}
Group:      Qt/Qt
License:    LGPLv3
URL:        https://www.qt.io
Source0:    %{name}-%{version}.tar.xz
BuildRequires:  qt5-qtcore-devel >= 5.12.7
BuildRequires:  qt5-qtgui-devel >= 5.12.7
BuildRequires:  qt5-qtopengl-devel >= 5.12.7
BuildRequires:  qt5-qtdeclarative-devel >= 5.12.7
BuildRequires:  qt5-qtdeclarative-qtquick-devel >= 5.12.7
BuildRequires:  qt5-qtdeclarative-devel-tools
BuildRequires:  qt5-qmake
BuildRequires:  fdupes

%description
Qt is a cross-platform application and UI framework. Using Qt, you can
write web-enabled applications once and deploy them across desktop,
mobile and embedded systems without rewriting the source code.
.
This package contains the Qt Graphical Effect library

%prep
%setup -q -n %{name}-%{version}/upstream

%build
export QTDIR=/usr/share/qt5
%qmake5
make %{?_smp_flags}

%install
rm -rf %{buildroot}
%qmake_install

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

#### File section

%files
%defattr(-,root,root,-)
%{_libdir}/qt5/qml/QtGraphicalEffects/*
